export interface Thumbnail {
  path: string;
  extension: string;
}

export interface Item {
  resourceURI: string;
  name: string;
}

export interface ICharacterComics {
  available: number;
  collectionURI: string;
  items: Item[];
  returned: number;
}

export interface Series {
  available: number;
  collectionURI: string;
  items: Item[];
  returned: number;
}

export interface Item3 {
  resourceURI: string;
  name: string;
  type: string;
}

export interface Stories {
  available: number;
  collectionURI: string;
  items: Item3[];
  returned: number;
}

export interface Events {
  available: number;
  collectionURI: string;
  items: Item[];
  returned: number;
}

export interface Url {
  type: string;
  url: string;
}

export interface Variant {
  resourceURI: string;
  name: string;
}

export interface ICharacter {
  id: number;
  name: string;
  description: string;
  modified: Date;
  thumbnail: Thumbnail;
  resourceURI: string;
  comics: ICharacterComics;
  series: Series;
  stories: Stories;
  events: Events;
  urls: Url[];
  hidden?: boolean;
}

export interface Price {
  type: string;
  price: number;
}

export interface Creators {
  available: number;
  collectionURI: string;
  items: Item[];
  returned: number;
}

export interface IComic {
  id: number;
  digitalId: number;
  title: string;
  issueNumber: number;
  variantDescription: string;
  description: string;
  modified: Date;
  isbn: string;
  upc: string;
  diamondCode: string;
  ean: string;
  issn: string;
  format: string;
  pageCount: number;
  textObjects: any[];
  resourceURI: string;
  urls: Url[];
  series: Series;
  variants: Variant[];
  collections: any[];
  collectedIssues: any[];
  dates: Date[];
  prices: Price[];
  thumbnail: Thumbnail;
  images: any[];
  creators: Creators;
  characters: ICharacterComics;
  stories: Stories;
  events: Events;
  hidden?: boolean;
}

export interface IStory {
  id: number;
  title: string;
  description: string;
  resourceURI: string;
  type: string;
  modified: Date;
  thumbnail?: Thumbnail;
  creators: Creators;
  characters: ICharacterComics;
  series: Series;
  comics: Creators;
  events: Events;
  originalIssue: Item;
  hidden?: boolean;
}

export interface IFilters {
  characterName?: string;
  comicTitle?: string;
  comics?: string;
  stories?: string;
}

export interface IApiResponse<T> {
  offset: number;
  limit: number;
  total: number;
  count: number;
  results: T[];
}

export type bookmarkItem = ICharacter | IComic | IStory;
export interface IBookmarkState {
  characters: ICharacter[];
  comics: IComic[];
  stories: IStory[];
}
