export enum EPages {
  characters = 'characters',
  comics = 'comics',
  stories = 'stories',
  bookmarks = 'bookmarks',
}
export enum EActionType {
  ADD = 'add',
  HIDE = 'hide',
  SHOW_ALL = 'show-all',
  ADD_ITEM = 'add-item',
  REMOVE_ITEM = 'remove-item',
  REMOVE_ALL = 'remove-all',
}

export enum EArrowAction {
  LEFT = 'left',
  RIGHT = 'right',
}

export enum ESearchParams {
  PAGE = 'page',
  NAME = 'name',
  STORY = 'story',
  COMIC = 'comic',
  TITLE = 'title',
  MAGAZINE = 'magazine',
  TRADE_PAPERBACK = 'trade paperback',
  HARDCOVER = 'hardcover',
  CHARACTER = 'characters',
  DIGEST = 'digest',
  GRAPHIC_NOVEL = 'graphic novel',
  DIGITAL_COMIC = 'digital comic',
  INFINITE_COMIC = 'infinite comic',
  FORMAT = 'format',
}
