export const resultsPerPage = 16;
export const maxResults = 1000;
export const maxOptionLength = 50;
export const REACT_APP_BASE_API_URL =
  'https://gateway.marvel.com:443/v1/public';
export const REACT_APP_API_PUBLIC_KEY3 = '0f95345d31f4249b16f96d6f568b7dec';
export const characters_base_url = `${REACT_APP_BASE_API_URL}/characters?apikey=${REACT_APP_API_PUBLIC_KEY3}&limit=${resultsPerPage}`;
export const comics_base_url = `${REACT_APP_BASE_API_URL}/comics?apikey=${REACT_APP_API_PUBLIC_KEY3}&limit=${resultsPerPage}`;
export const stories_base_url = `${REACT_APP_BASE_API_URL}/stories?apikey=${REACT_APP_API_PUBLIC_KEY3}&limit=${resultsPerPage}`;
