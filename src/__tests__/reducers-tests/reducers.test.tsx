import { EActionType } from '../../ts/enums';
import {
  bookmarkReducer,
  charactersReducer,
  comicsReducer,
  storiesReducer,
} from '../../redux/reducers/reducer';

import { fakeCharacters, fakeComics, fakeStories } from '../../mocks/mockData';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { store } from '../../redux/reducers/store';
import { mockCharacters } from '../../mocks/mockData';
import * as utils from '../../services/utils';
import { render } from '@testing-library/react';
import { IBookmarkState } from '../../ts/interfaces';

const initialBookmarkState: IBookmarkState = {
  characters: [...fakeCharacters],
  comics: [...fakeComics],
  stories: [...fakeStories],
};

function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>{element}</BrowserRouter>
    </Provider>
  );
}
describe('Reducers', () => {
  test('Should set the characters', () => {
    expect(
      charactersReducer(undefined, {
        type: EActionType.ADD,
        payload: fakeCharacters,
      })
    ).toEqual(fakeCharacters);
  });

  test('Should  hide  a character', () => {
    const state = charactersReducer(fakeCharacters, {
      type: EActionType.HIDE,
      payload: fakeCharacters[0].id,
    });
    expect(state[0].hidden).toBe(true);
    expect(state[1].hidden).toBe(undefined);
  });
  test('Show all characters', () => {
    const state = charactersReducer(fakeCharacters, {
      type: EActionType.SHOW_ALL,
    });
    for (let index = 0; index < state.length; index++) {
      expect(state[0].hidden).toBe(false);
    }
  });

  test('Should add comics', () => {
    expect(
      comicsReducer(fakeComics, { type: EActionType.ADD, payload: fakeComics })
    ).toEqual(fakeComics);
  });
  test('Should  hide  a comic', () => {
    const state = comicsReducer(fakeComics, {
      type: EActionType.HIDE,
      payload: fakeComics[0].id,
    });
    expect(state[0].hidden).toBe(true);
    expect(state[1].hidden).toBe(undefined);
  });
  test('Show all comics', () => {
    const state = comicsReducer(fakeComics, {
      type: EActionType.SHOW_ALL,
    });
    for (let index = 0; index < state.length; index++) {
      expect(state[0].hidden).toBe(false);
    }
  });
  test('Should add stories', () => {
    expect(
      storiesReducer(fakeStories, {
        type: EActionType.ADD,
        payload: fakeStories,
      })
    ).toEqual(fakeStories);
  });
  test('Should  hide  a stories', () => {
    const state = storiesReducer(fakeStories, {
      type: EActionType.HIDE,
      payload: fakeStories[0].id,
    });
    expect(state[0].hidden).toBe(true);
    expect(state[1].hidden).toBe(undefined);
  });
  test('Show all stories', () => {
    const state = storiesReducer(fakeStories, {
      type: EActionType.SHOW_ALL,
    });
    for (let index = 0; index < state.length; index++) {
      expect(state[0].hidden).toBe(false);
    }
  });

  test('Should bookmark a character', () => {
    const isCharacterMock = jest.spyOn(utils, 'isCharacter');
    isCharacterMock.mockImplementation(() => true);
    const state = bookmarkReducer(initialBookmarkState, {
      type: EActionType.ADD_ITEM,
      payload: fakeCharacters[0],
    });

    expect(state.characters[0]).toEqual(fakeCharacters[0]);
  });
  test('Should bookmark a comic', () => {
    const isComicMock = jest.spyOn(utils, 'isComic');
    isComicMock.mockImplementation(() => true);
    const state = bookmarkReducer(initialBookmarkState, {
      type: EActionType.ADD_ITEM,
      payload: fakeComics[0],
    });

    expect(state.comics[0]).toEqual(fakeComics[0]);
  });
  test('Should bookmark a story', () => {
    const isComicMock = jest.spyOn(utils, 'isComic');
    isComicMock.mockImplementation(() => false);
    const isCharacterMock = jest.spyOn(utils, 'isCharacter');
    isCharacterMock.mockImplementation(() => false);
    const state = bookmarkReducer(initialBookmarkState, {
      type: EActionType.ADD_ITEM,
      payload: fakeStories[0],
    });
    expect(state.stories[0]).toEqual(fakeStories[0]);
  });

  test('Should remove a bookmarked character', () => {
    const isCharacterMock = jest.spyOn(utils, 'isCharacter');
    isCharacterMock.mockImplementation(() => true);
    const state = bookmarkReducer(initialBookmarkState, {
      type: EActionType.REMOVE_ITEM,
      payload: fakeCharacters[1],
    });

    expect(state).not.toContain(fakeCharacters[1]);
  });
  test('Should remove a bookmarked comic', () => {
    const isComicMock = jest.spyOn(utils, 'isComic');
    isComicMock.mockImplementation(() => true);
    const state = bookmarkReducer(initialBookmarkState, {
      type: EActionType.REMOVE_ITEM,
      payload: fakeComics[1],
    });

    expect(state).not.toContain(fakeComics[1]);
  });
  test('Should remove a bookmarked story', () => {
    const isComicMock = jest.spyOn(utils, 'isComic');
    isComicMock.mockImplementation(() => false);
    const isCharacterMock = jest.spyOn(utils, 'isCharacter');
    isCharacterMock.mockImplementation(() => false);
    const state = bookmarkReducer(initialBookmarkState, {
      type: EActionType.REMOVE_ITEM,
      payload: fakeStories[1],
    });
    expect(state).not.toContain(fakeStories[1]);
  });
  test('Should remove all bookmarks', () => {
    const state = bookmarkReducer(initialBookmarkState, {
      type: EActionType.REMOVE_ALL,
    });
    expect(state.characters).toEqual([]);
    expect(state.stories).toEqual([]);
    expect(state.comics).toEqual([]);
  });
});
