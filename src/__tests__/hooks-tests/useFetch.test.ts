import { useFetch } from '../../hooks/useFetch';
import fetchMock from 'fetch-mock';
import 'whatwg-fetch';
import { renderHook, waitFor } from '@testing-library/react';

beforeAll(() => {
  global.fetch = fetch;
});
afterAll(() => {
  fetchMock.restore();
});

describe('useFetch', () => {
  test('Should fetch successfully data from an API', async () => {
    const { result } = renderHook(() => useFetch());
    fetchMock.mock('test.com', {
      returnedData: 'test data',
    });
    await waitFor(async () => {
      const data = await result.current.fetchData('test.com');
      expect(data).toEqual('test data');
    });
  });
  test('Shouldfetch with error', async () => {
    const { result } = renderHook(() => useFetch());
    // mockAxios.get.mockRejectedValueOnce(new Error('error'));
    await waitFor(async () => {
      result.current.fetchData('/react');
    });
    expect(result.current.error).toBeTruthy();
  });
});
