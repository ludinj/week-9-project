import { calculateOffset } from '../../services/utils';

test('properly calculates the offset param', () => {
  expect(calculateOffset(0)).toBe(0);
  expect(calculateOffset(2)).toBe(16);
});
