import { render, screen, waitFor } from '@testing-library/react';

import { Provider } from 'react-redux';
import { store } from '../../redux/reducers/store';
import { BrowserRouter } from 'react-router-dom';
import userEvent from '@testing-library/user-event';
import ComicDetails from '../../pages/comic-details/comic-details';
import StoryDetails from '../../pages/story-details/story-details';
function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>{element}</BrowserRouter>
    </Provider>
  );
}

describe('StoryDetails', () => {
  test('Should render story details', async () => {
    renderWithContext(<StoryDetails />);
    expect(screen.getByText('Loading...')).toBeInTheDocument();
    await waitFor(() => {
      expect(
        screen.getByText(/Investigating the murder of a teenage girl/i)
      ).toBeInTheDocument();
    });
    await waitFor(() => {
      expect(screen.getByText(/bookmark/i)).toBeInTheDocument();
    });
  });

  test('Should bookmark a item', async () => {
    renderWithContext(<StoryDetails />);
    await waitFor(() => {
      expect(
        screen.getByText(/Investigating the murder of a teenage girl/i)
      ).toBeInTheDocument();
    });
    const button = screen.getByText(/bookmark/i);
    expect(button).not.toBeDisabled();
    userEvent.click(button);
    jest.spyOn(window, 'alert').mockImplementation();
    await waitFor(() => {
      expect(window.alert).toBeCalledWith('Item Bookmarked');
    });
    await waitFor(() => {
      expect(button).toBeDisabled();
    });
  });
});
