import { render, screen, waitFor } from '@testing-library/react';
import Characters from '../../pages/charaters/characters';
import { Provider } from 'react-redux';
import { store } from '../../redux/reducers/store';
import { BrowserRouter } from 'react-router-dom';
import userEvent from '@testing-library/user-event';

function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>{element}</BrowserRouter>
    </Provider>
  );
}
afterAll(() => {
  jest.clearAllMocks();
});

describe('<Characters/>', () => {
  test('Should render list of characters', async () => {
    renderWithContext(<Characters />);
    expect(screen.getByText('Loading..')).toBeInTheDocument();
    await waitFor(() => {
      expect(screen.getByText('3-D Man')).toBeInTheDocument();
    });
    await waitFor(() => {
      expect(screen.getByText(/Agent Brand/i)).toBeInTheDocument();
    });
  });

  test('Should search by name', async () => {
    renderWithContext(<Characters />);
    const searchInput = screen.getByPlaceholderText('Search a character...');
    userEvent.type(searchInput, 'hulk');
    await waitFor(() => {
      const params = window.location.href;
      console.log(params);
      expect(searchInput).toHaveValue('hulk');
    });
  });
  test('Should filter by comic', async () => {
    renderWithContext(<Characters />);

    await waitFor(() => {
      expect(screen.getByLabelText('Comics.')).toBeInTheDocument();
    });
  });
});
