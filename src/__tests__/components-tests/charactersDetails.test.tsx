import { render, screen, waitFor } from '@testing-library/react';
import CharacterDetails from '../../pages/character-details/character-details';
import { Provider } from 'react-redux';
import { store } from '../../redux/reducers/store';
import { BrowserRouter } from 'react-router-dom';
import userEvent from '@testing-library/user-event';
import ComicDetails from '../../pages/comic-details/comic-details';

function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>{element}</BrowserRouter>
    </Provider>
  );
}

describe('<CharacterDetails />', () => {
  test('Should render characters details', async () => {
    renderWithContext(<CharacterDetails />);
    expect(screen.getByText('Loading...')).toBeInTheDocument();
    await waitFor(() => {
      expect(screen.getByText('3-D Man')).toBeInTheDocument();
    });
    await waitFor(() => {
      expect(screen.getByText(/bookmark/i)).toBeInTheDocument();
    });
  });

  test('Should bookmark a item', async () => {
    renderWithContext(<CharacterDetails />);

    expect(screen.getByText('Loading...')).toBeInTheDocument();

    await waitFor(() => {
      expect(screen.getByText('3-D Man')).toBeInTheDocument();
    });
    const button = screen.getByText(/bookmark/i);

    expect(button).not.toBeDisabled();

    userEvent.click(button);
    jest.spyOn(window, 'alert').mockImplementation();
    await waitFor(() => {
      expect(window.alert).toBeCalledWith('Item Bookmarked');
    });
    await waitFor(() => {
      expect(button).toBeDisabled();
    });
  });
});
