import { render, screen, waitFor } from '@testing-library/react';
import Stories from '../../pages/stories/stories';
import { Provider } from 'react-redux';
import { store } from '../../redux/reducers/store';
import { BrowserRouter } from 'react-router-dom';

function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>{element}</BrowserRouter>
    </Provider>
  );
}

describe('<Comics />', () => {
  test('Should render list of comics', async () => {
    renderWithContext(<Stories />);
    expect(screen.getByText('Loading..')).toBeInTheDocument();
    await waitFor(() => {
      expect(
        screen.getByText(/Investigating the murder of a teenage girl/i)
      ).toBeInTheDocument();
    });
    await waitFor(() => {
      expect(
        screen.getByText(/Ordinary New York City cop Frankie/i)
      ).toBeInTheDocument();
    });
  });
});
