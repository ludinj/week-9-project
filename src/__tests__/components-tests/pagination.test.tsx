import Pagination from '../../components/pagination/Pagination';
import { render, screen, waitFor } from '@testing-library/react';

import { Provider } from 'react-redux';
import { store } from '../../redux/reducers/store';
import { BrowserRouter } from 'react-router-dom';
import userEvent from '@testing-library/user-event';
window.scroll = jest.fn();

function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>{element}</BrowserRouter>
    </Provider>
  );
}
describe('<Pagination/>', () => {
  afterAll(() => {
    jest.clearAllMocks();
  });

  test('Should navigate to next page', async () => {
    renderWithContext(<Pagination totalResults={100} currentPage={0} />);
    const nextButton = screen.getByText('Next');
    userEvent.click(nextButton);
    await waitFor(() => {
      const pageParam = global.window.location.href.split('?')[1];
      expect(pageParam).toMatch('page=1');
    });
  });
  test('Should navigate to previous page', async () => {
    renderWithContext(<Pagination totalResults={100} currentPage={6} />);
    const previousButton = screen.getByText('Previous');
    userEvent.click(previousButton);
    await waitFor(() => {
      const pageParam = global.window.location.href.split('?')[1];
      expect(pageParam).toMatch('page=5');
    });
  });
  test('Should navigate to a specific  page', async () => {
    renderWithContext(<Pagination totalResults={100} currentPage={6} />);
    const button = screen.getByText('7');
    userEvent.click(button);
    await waitFor(() => {
      const pageParam = global.window.location.href.split('?')[1];
      expect(pageParam).toMatch('page=7');
    });
  });
});
