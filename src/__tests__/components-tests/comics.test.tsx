import { render, screen, waitFor } from '@testing-library/react';
import Comics from '../../pages/comics/comics';
import { Provider } from 'react-redux';
import { store } from '../../redux/reducers/store';
import { BrowserRouter } from 'react-router-dom';
import userEvent from '@testing-library/user-event';

function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>{element}</BrowserRouter>
    </Provider>
  );
}

describe('<Comics />', () => {
  test('Should render list of comics', async () => {
    renderWithContext(<Comics />);
    expect(screen.getByText('Loading..')).toBeInTheDocument();
    await waitFor(() => {
      expect(
        screen.getByText('Startling Stories: The Incorrigible Hulk (2004) #1')
      ).toBeInTheDocument();
    });
    await waitFor(() => {
      expect(screen.getByText('Marvel Previews (2017)')).toBeInTheDocument();
    });
  });
});
