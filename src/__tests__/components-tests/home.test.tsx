import { render, screen, waitFor } from '@testing-library/react';
import Home from '../../pages/home/home';
import { Provider } from 'react-redux';
import { store } from '../../redux/reducers/store';
import { BrowserRouter, useLocation } from 'react-router-dom';
import userEvent from '@testing-library/user-event';

const LocationDisplay = () => {
  const location = useLocation();

  return <div data-testid='location-display'>{location.pathname}</div>;
};

function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>
        {element}
        <LocationDisplay />
      </BrowserRouter>
    </Provider>
  );
}

describe('<Home/>', () => {
  test('Should display explore button', () => {
    renderWithContext(<Home />);

    expect(screen.getByText('Explore')).toBeInTheDocument();
  });
  test('Should navigate to characters page explore button', async () => {
    renderWithContext(<Home />);
    const exploreButton = screen.getByText('Explore');
    userEvent.click(exploreButton);

    await waitFor(() => {
      expect(screen.getByTestId('location-display')).toHaveTextContent(
        '/characters'
      );
    });
  });
});
