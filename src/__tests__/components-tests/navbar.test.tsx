import Navbar from '../../components/navbar/navbar';
import { render, screen, waitFor } from '@testing-library/react';
import { useLocation } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../../redux/reducers/store';
import { BrowserRouter } from 'react-router-dom';
import userEvent from '@testing-library/user-event';

export const LocationDisplay = () => {
  const location = useLocation();

  return <div data-testid='location-display'>{location.pathname}</div>;
};

function renderWithContext(element: React.ReactElement) {
  render(
    <Provider store={store}>
      <BrowserRouter>
        {element}
        <LocationDisplay />
      </BrowserRouter>
    </Provider>
  );
}
describe('<Navbar/>', () => {
  test('Should navigate to characters page', async () => {
    renderWithContext(<Navbar />);

    const link = screen.getByText('Characters');
    userEvent.click(link);

    await waitFor(() => {
      expect(screen.getByTestId('location-display')).toHaveTextContent(
        '/characters'
      );
    });
  });
  test('Should navigate to comics page', async () => {
    renderWithContext(<Navbar />);

    const link = screen.getByText('Comics');
    userEvent.click(link);

    await waitFor(() => {
      expect(screen.getByTestId('location-display')).toHaveTextContent(
        '/comics'
      );
    });
  });
  test('Should navigate to stories page', async () => {
    renderWithContext(<Navbar />);

    const link = screen.getByText('Stories');
    userEvent.click(link);

    await waitFor(() => {
      expect(screen.getByTestId('location-display')).toHaveTextContent(
        '/stories'
      );
    });
  });
  test('Should navigate to bookmark page', async () => {
    renderWithContext(<Navbar />);

    const link = screen.getByText('Bookmarks');
    userEvent.click(link);

    await waitFor(() => {
      expect(screen.getByTestId('location-display')).toHaveTextContent(
        '/bookmarks'
      );
    });
  });
});
