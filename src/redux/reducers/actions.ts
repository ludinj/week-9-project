import { ICharacter, IComic, IStory } from '../../ts/interfaces';
import { EActionType } from '../../ts/enums';

interface ISetAction<T> {
  type: EActionType.ADD;
  payload: T[];
}

interface IHideAction {
  type: EActionType.HIDE;
  payload: number;
}

interface IShowAction {
  type: EActionType.SHOW_ALL;
}

interface IAddItemAction {
  type: EActionType.ADD_ITEM;
  payload: IStory | ICharacter | IComic;
}
interface IRemoveItemAction {
  type: EActionType.REMOVE_ITEM;
  payload: IStory | ICharacter | IComic;
}

interface IRemoveAllItemsAction {
  type: EActionType.REMOVE_ALL;
}

export type CharactersActions =
  | ISetAction<ICharacter>
  | IHideAction
  | IShowAction;

export type ComicsActions = ISetAction<IComic> | IHideAction | IShowAction;
export type StoriesActions = ISetAction<IStory> | IHideAction | IShowAction;
export type BookmarkActions =
  | IAddItemAction
  | IRemoveItemAction
  | IRemoveAllItemsAction;
