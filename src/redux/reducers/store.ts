import { combineReducers, createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import thunk from 'redux-thunk';
import {
  bookmarkReducer,
  charactersReducer,
  comicsReducer,
  storiesReducer,
} from './reducer';

const persistConfig = {
  key: 'root',
  whitelist: ['bookmarks'],
  storage,
};

const rootReducer = combineReducers({
  characters: charactersReducer,
  comics: comicsReducer,
  stories: storiesReducer,
  bookmarks: bookmarkReducer,
});
const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(persistedReducer, {}, applyMiddleware(thunk));
export const persistor = persistStore(store);
export type RootState = ReturnType<typeof rootReducer>;
