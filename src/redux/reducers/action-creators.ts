import { Dispatch } from 'redux';
import { EActionType } from '../../ts/enums';
import { ICharacter, IComic, IStory } from '../../ts/interfaces';
import {
  BookmarkActions,
  CharactersActions,
  ComicsActions,
  StoriesActions,
} from './actions';

export const setCharactersAction = (characters: ICharacter[]) => {
  return (dispatch: Dispatch<CharactersActions>) => {
    dispatch({
      type: EActionType.ADD,
      payload: characters,
    });
  };
};

export const hideCharacterAction = (id: number) => {
  return (dispatch: Dispatch<CharactersActions>) => {
    dispatch({
      type: EActionType.HIDE,
      payload: id,
    });
  };
};

export const showAllCharactersAction = () => {
  return (dispatch: Dispatch<CharactersActions>) => {
    dispatch({
      type: EActionType.SHOW_ALL,
    });
  };
};

export const setComicsAction = (characters: IComic[]) => {
  return (dispatch: Dispatch<ComicsActions>) => {
    dispatch({
      type: EActionType.ADD,
      payload: characters,
    });
  };
};

export const hideComicAction = (id: number) => {
  return (dispatch: Dispatch<ComicsActions>) => {
    dispatch({
      type: EActionType.HIDE,
      payload: id,
    });
  };
};

export const showAllComicsAction = () => {
  return (dispatch: Dispatch<ComicsActions>) => {
    dispatch({
      type: EActionType.SHOW_ALL,
    });
  };
};
export const setStoriesAction = (stories: IStory[]) => {
  return (dispatch: Dispatch<StoriesActions>) => {
    dispatch({
      type: EActionType.ADD,
      payload: stories,
    });
  };
};

export const hideStoryAction = (id: number) => {
  return (dispatch: Dispatch<StoriesActions>) => {
    dispatch({
      type: EActionType.HIDE,
      payload: id,
    });
  };
};

export const showAllStoriesAction = () => {
  return (dispatch: Dispatch<StoriesActions>) => {
    dispatch({
      type: EActionType.SHOW_ALL,
    });
  };
};

export const addBookmarkAction = (item: IComic | IStory | ICharacter) => {
  return (dispatch: Dispatch<BookmarkActions>) => {
    dispatch({
      type: EActionType.ADD_ITEM,
      payload: item,
    });
  };
};
export const removeBookmarkAction = (item: IComic | IStory | ICharacter) => {
  return (dispatch: Dispatch<BookmarkActions>) => {
    dispatch({
      type: EActionType.REMOVE_ITEM,
      payload: item,
    });
  };
};
export const removeAllBookmarksAction = () => {
  return (dispatch: Dispatch<BookmarkActions>) => {
    dispatch({
      type: EActionType.REMOVE_ALL,
    });
  };
};
