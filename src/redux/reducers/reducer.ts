import {
  IBookmarkState,
  ICharacter,
  IComic,
  IStory,
} from '../../ts/interfaces';
import { EActionType } from '../../ts/enums';

import {
  BookmarkActions,
  CharactersActions,
  ComicsActions,
  StoriesActions,
} from './actions';
import { isCharacter, isComic } from '../../services/utils';

const initialBookmarkState: IBookmarkState = {
  characters: [],
  comics: [],
  stories: [],
};
export const charactersReducer = (
  state: ICharacter[] = [],
  action: CharactersActions
): ICharacter[] => {
  switch (action.type) {
    case EActionType.ADD:
      return action.payload;
    case EActionType.HIDE: {
      const newState = [...state];
      const idex = newState.findIndex(
        (character) => character.id === action.payload
      );
      newState[idex].hidden = true;
      return [...newState];
    }
    case EActionType.SHOW_ALL: {
      const newState = [...state];
      newState.map((character) => (character.hidden = false));
      return [...newState];
    }

    default:
      return state;
  }
};

export const comicsReducer = (
  state: IComic[] = [],
  action: ComicsActions
): IComic[] => {
  switch (action.type) {
    case EActionType.ADD:
      return action.payload;
    case EActionType.HIDE: {
      const newState = [...state];
      const idex = newState.findIndex((comic) => comic.id === action.payload);
      newState[idex].hidden = true;
      return [...newState];
    }
    case EActionType.SHOW_ALL: {
      const newState = [...state];
      newState.map((comic) => (comic.hidden = false));
      return [...newState];
    }

    default:
      return state;
  }
};
export const storiesReducer = (
  state: IStory[] = [],
  action: StoriesActions
): IStory[] => {
  switch (action.type) {
    case EActionType.ADD:
      return action.payload;
    case EActionType.HIDE: {
      const newState = [...state];
      const idex = newState.findIndex((comic) => comic.id === action.payload);
      newState[idex].hidden = true;
      return newState;
    }
    case EActionType.SHOW_ALL: {
      const newState = [...state];
      newState.map((comic) => (comic.hidden = false));
      return newState;
    }

    default:
      return state;
  }
};

export const bookmarkReducer = (
  state: IBookmarkState = initialBookmarkState,
  action: BookmarkActions
) => {
  switch (action.type) {
    case EActionType.ADD_ITEM: {
      if (isCharacter(action.payload)) {
        const newState = { ...state };
        const itemExist = newState.characters.some(
          (character) => character.id === action.payload.id
        );
        if (!itemExist) {
          newState.characters.push(action.payload);

          return newState;
        } else {
          return state;
        }
      } else if (isComic(action.payload)) {
        const newState = { ...state };
        const itemExist = newState.comics.some(
          (comic) => comic.id === action.payload.id
        );
        if (!itemExist) {
          newState.comics.push(action.payload);

          return newState;
        } else {
          return state;
        }
      } else {
        const newState = { ...state };
        const itemExist = newState.stories.some(
          (story) => story.id === action.payload.id
        );
        if (!itemExist) {
          newState.stories.push(action.payload);

          return newState;
        } else {
          return state;
        }
      }
    }

    case EActionType.REMOVE_ITEM: {
      if (isCharacter(action.payload)) {
        const newState = { ...state };
        const characterIdex = newState.characters.findIndex(
          (character) => character.id === action.payload.id
        );
        newState.characters.splice(characterIdex, 1);
        return newState;
      } else if (isComic(action.payload)) {
        const newState = { ...state };
        const comicIdex = newState.comics.findIndex(
          (comic) => comic.id === action.payload.id
        );
        newState.comics.splice(comicIdex, 1);
        return newState;
      } else {
        const newState = { ...state };
        const storyIdex = newState.stories.findIndex(
          (story) => story.id === action.payload.id
        );
        newState.stories.splice(storyIdex, 1);
        return newState;
      }
    }
    case EActionType.REMOVE_ALL: {
      const newState: IBookmarkState = {
        characters: [],
        stories: [],
        comics: [],
      };

      return newState;
    }
    default:
      return state;
  }
};
