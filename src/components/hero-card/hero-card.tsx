import { ICharacter } from '../../ts/interfaces';
import { useNavigate } from 'react-router-dom';

import noImage from '../../assets/noImage.png';

import './hero-card.scss';
import { EPages } from '../../ts/enums';
type CardProps = {
  character: ICharacter;
};
const HeroCard = ({ character }: CardProps) => {
  const navigate = useNavigate();
  const { thumbnail, name, id } = character;

  const handleClick = () => {
    navigate(`/${EPages.characters}/${id}`);
  };

  return (
    <div className='character__card'>
      <div className='character' onClick={handleClick}>
        {thumbnail ? (
          <img src={thumbnail.path + '.' + thumbnail.extension} alt={name} />
        ) : (
          <img src={noImage} alt='noImage'></img>
        )}
        <div className='character__info'>
          <h3>{name}</h3>
        </div>
      </div>
    </div>
  );
};

export default HeroCard;
