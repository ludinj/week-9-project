import React from 'react';
import { IComic } from '../../ts/interfaces';
import { useNavigate } from 'react-router-dom';

import noImage from '../../assets/noImage.png';
import './comic-card.scss';
import { EPages } from '../../ts/enums';
type CardProps = {
  comic: IComic;
};
const ComicCard = ({ comic }: CardProps) => {
  const navigate = useNavigate();
  const { thumbnail, title, id } = comic;

  const handleClick = () => {
    navigate(`/${EPages.comics}/${id}`);
  };

  return (
    <div className='comic__card'>
      <div className='comic' onClick={handleClick}>
        {thumbnail ? (
          <img src={thumbnail.path + '.' + thumbnail.extension} alt={title} />
        ) : (
          <img src={noImage} alt='noImage'></img>
        )}
        <div className='comic__info'>
          <h3>{title}</h3>
        </div>
      </div>
      <div className='buttons'></div>
    </div>
  );
};

export default ComicCard;
