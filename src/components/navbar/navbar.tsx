import marvelLogo from '../../assets/logoNo2.png';
import './navbar.scss';
import { Link } from 'react-router-dom';
import { EPages } from '../../ts/enums';
const Navbar = () => {
  return (
    <div className='navbar'>
      <div className='logo'>
        <Link to={'/'}>
          <img src={marvelLogo} alt='' />
        </Link>
      </div>

      <ul className='navigation'>
        <Link to={`${EPages.characters}`}>
          <li>Characters</li>
        </Link>
        <Link to={`${EPages.comics}`}>
          <li>Comics</li>
        </Link>
        <Link to={`${EPages.stories}`}>
          <li>Stories</li>
        </Link>
        <Link to={`${EPages.bookmarks}`}>
          <li>Bookmarks</li>
        </Link>
      </ul>
    </div>
  );
};

export default Navbar;
