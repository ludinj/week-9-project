import React from 'react';
import './footer.scss';
import MLogo from '../../assets/MLogo.png';

const Footer = () => {
  return (
    <footer className='footer'>
      <div className='footer__content'>
        <img src={MLogo} alt='' />
        <p> © 2022 All Rights Reserved.</p>
        <div className='icons'></div>
      </div>
    </footer>
  );
};

export default Footer;
