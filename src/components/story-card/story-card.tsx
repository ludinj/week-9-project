import React from 'react';
import { IStory } from '../../ts/interfaces';
import { useNavigate } from 'react-router-dom';
import noImage from '../../assets/noImage.png';
import './story-card.scss';
import { EPages } from '../../ts/enums';
type CardProps = {
  story: IStory;
};
const StoryCard = ({ story }: CardProps) => {
  const navigate = useNavigate();
  const { thumbnail, title, id } = story;

  const handleClick = () => {
    navigate(`/${EPages.stories}/${id}`);
  };

  return (
    <div className='story__card'>
      <div className='story' onClick={handleClick}>
        <img
          src={thumbnail ? thumbnail.path + '.' + thumbnail.extension : noImage}
          alt={title}
        />
        <div className='story__info'>
          <h3>{title}</h3>
        </div>
      </div>
    </div>
  );
};

export default StoryCard;
