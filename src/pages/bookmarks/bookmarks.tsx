import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import { RootState } from '../../redux/reducers/store';
import {
  removeAllBookmarksAction,
  removeBookmarkAction,
} from '../../redux/reducers/action-creators';
import HeroCard from '../../components/hero-card/hero-card';
import ComicCard from '../../components/comic-card/comic-card';
import StoryCard from '../../components/story-card/story-card';
import './bookmarks.scss';

const Bookmarks = () => {
  const dispatch = useDispatch();
  const removeBookmarkItem = bindActionCreators(removeBookmarkAction, dispatch);
  const removeAllBookmark = bindActionCreators(
    removeAllBookmarksAction,
    dispatch
  );
  const bookmarks = useSelector((state: RootState) => state.bookmarks);

  return (
    <div className='bookmarks'>
      <div className='bookmarks__container'>
        <button onClick={removeAllBookmark}>Remove all</button>
        <div className='favorite-characters'>
          <h2>Your Favorites Characters:</h2>
          <div className='grid'>
            {bookmarks.characters.length > 0 ? (
              <>
                {bookmarks.characters.map((character) => (
                  <div key={character.id}>
                    <button onClick={() => removeBookmarkItem(character)}>
                      Remove
                    </button>
                    <HeroCard character={character} />
                  </div>
                ))}
              </>
            ) : (
              <p>No characters added yet</p>
            )}
          </div>
        </div>
        <div className='favorite-comics'>
          <h2>Your Favorites Comics:</h2>

          <div className='grid'>
            {bookmarks.comics.length > 0 ? (
              <>
                {bookmarks.comics.map((comic) => (
                  <div key={comic.id}>
                    <button onClick={() => removeBookmarkItem(comic)}>
                      Remove
                    </button>
                    <ComicCard comic={comic} />
                  </div>
                ))}
              </>
            ) : (
              <p>No comics added yet</p>
            )}
          </div>
        </div>
        <div className='favorite-stories'>
          <h2>Your Favorites Stories:</h2>
          <div className='grid'>
            {bookmarks.stories.length > 0 ? (
              <>
                {bookmarks.stories.map((story) => (
                  <div key={story.id}>
                    <button onClick={() => removeBookmarkItem(story)}>
                      Remove
                    </button>
                    <StoryCard story={story} />
                  </div>
                ))}
              </>
            ) : (
              <p>No stories added yet.</p>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Bookmarks;
