import React, { useEffect, useState } from 'react';

import { useParams } from 'react-router-dom';
import { IStory } from '../../ts/interfaces';
import { useNavigate } from 'react-router-dom';
import noImage from '../../assets/noImage.png';

import './story-details.scss';

import { useFetch } from '../../hooks/useFetch';
import { addBookmarkAction } from '../../redux/reducers/action-creators';
import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  REACT_APP_API_PUBLIC_KEY3,
  REACT_APP_BASE_API_URL,
} from '../../services/constants';
const StoryDetails = () => {
  const { fetchData } = useFetch();
  const dispatch = useDispatch();
  const { storyId } = useParams();
  const navigate = useNavigate();
  const bookmarkItem = bindActionCreators(addBookmarkAction, dispatch);
  const [story, setStory] = useState<IStory | undefined>(undefined);
  const [isDisableButton, SetIsDisableButton] = useState<boolean>(false);
  useEffect(() => {
    const getStory = async () => {
      try {
        const response = await fetchData(
          `${REACT_APP_BASE_API_URL}/stories/${storyId}?apikey=${REACT_APP_API_PUBLIC_KEY3}`
        );
        const story = response.results[0];
        setStory(story);
      } catch (error) {
        navigate('*');
      }
    };
    getStory();
  }, [storyId, navigate, fetchData]);
  const handleAddToBookmarks = () => {
    if (story) {
      bookmarkItem(story);
      SetIsDisableButton(true);
      alert('Item Bookmarked');
    }
  };
  return (
    <div className='story-details__page'>
      {story ? (
        <div className='story-details__container'>
          <section className='image__section'>
            <img
              src={
                story.thumbnail
                  ? story.thumbnail.path + '.' + story.thumbnail.extension
                  : noImage
              }
              alt={story.title}
            />
            <section className='info__section'>
              <h1>{story.title}</h1>
              {story.description ? (
                <p>{story.description}</p>
              ) : (
                <p>No description available</p>
              )}
              <button
                onClick={handleAddToBookmarks}
                className='bookmark'
                disabled={isDisableButton}
              >
                bookmark
              </button>
            </section>
          </section>

          <div className='story__appearances'>
            <div>
              <h4>Characters</h4>
              <ul>
                {story.characters.items.map((item, idx) => (
                  <li key={idx}>{item.name}</li>
                ))}
              </ul>
            </div>
            <div>
              <h4>Comics</h4>
              <ul>
                {story.comics.items.map((item, idx) => (
                  <li key={idx}>{item.name}</li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      ) : (
        <h2>Loading...</h2>
      )}
    </div>
  );
};

export default StoryDetails;
