import { Link } from 'react-router-dom';
import homeBg from '../../assets/homeBg.jpg';
import { EPages } from '../../ts/enums';
import './home.scss';
const Home = () => {
  return (
    <div className='home__container'>
      <div className='background__image'>
        <img src={homeBg} alt='' />
      </div>
      <div className='information'>
        <h3>Welcome to the wonderful world of comics!</h3>
        <p>
          Explore our huge gallery of marvel comics and fallow your favorite
          character new adventures.
        </p>
        <div className='information__button'>
          <Link to={`${EPages.characters}`}>Explore</Link>
        </div>
      </div>
    </div>
  );
};

export default Home;
