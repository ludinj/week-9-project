import React, { useEffect, useState } from 'react';

import { useParams } from 'react-router-dom';
import { ICharacter } from '../../ts/interfaces';
import { useNavigate } from 'react-router-dom';
import './character-details.scss';

import { useFetch } from '../../hooks/useFetch';
import { bindActionCreators } from 'redux';
import { useDispatch } from 'react-redux';
import { addBookmarkAction } from '../../redux/reducers/action-creators';
import {
  REACT_APP_API_PUBLIC_KEY3,
  REACT_APP_BASE_API_URL,
} from '../../services/constants';
const CharacterDetails = () => {
  const { fetchData } = useFetch();

  const { characterId } = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [character, setCharacter] = useState<ICharacter | undefined>(undefined);
  const [isDisableButton, SetIsDisableButton] = useState<boolean>(false);
  const bookmarkItem = bindActionCreators(addBookmarkAction, dispatch);
  useEffect(() => {
    const getCharacter = async () => {
      try {
        const response = await fetchData(
          `${REACT_APP_BASE_API_URL}/characters/${characterId}?apikey=${REACT_APP_API_PUBLIC_KEY3}`
        );

        const character = response.results[0];

        setCharacter(character);
      } catch (error) {
        navigate('*');
      }
    };
    getCharacter();
  }, [characterId, navigate, fetchData]);

  const handleAddToBookmarks = () => {
    if (character) {
      bookmarkItem(character);
      SetIsDisableButton(true);
      alert('Item Bookmarked');
    }
  };

  return (
    <div className='character-details__page'>
      {character ? (
        <div className='character-details__container'>
          <section className='image__section'>
            <img
              src={
                character.thumbnail.path + '.' + character.thumbnail.extension
              }
              alt={character.name}
            />
            <section className='info__section'>
              <h1>{character.name}</h1>
              {character.description ? (
                <p>{character.description}</p>
              ) : (
                <p>No description available</p>
              )}
              <button
                onClick={handleAddToBookmarks}
                className='bookmark'
                disabled={isDisableButton}
              >
                Bookmark
              </button>
            </section>
          </section>

          <div className='character__appearances'>
            <div>
              <h4>Comics</h4>
              <ul>
                {character.comics.items.map((item, idx) => (
                  <li key={idx}>{item.name}</li>
                ))}
              </ul>
            </div>
            <div>
              <h4>Stories</h4>
              <ul>
                {character.stories.items.map((item, idx) => (
                  <li key={idx}>{item.name}</li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      ) : (
        <h2>Loading...</h2>
      )}
    </div>
  );
};

export default CharacterDetails;
