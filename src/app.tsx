import { Routes, Route } from 'react-router-dom';
import Characters from './pages/charaters/characters';
import Comics from './pages/comics/comics';
import Home from './pages/home/home';
import Stories from './pages/stories/stories';
import { EPages } from './ts/enums';
import CharacterDetails from './pages/character-details/character-details';
import ComicDetails from './pages/comic-details/comic-details';
import StoryDetails from './pages/story-details/story-details';
import PageNotFound from './components/notFound/no-found';
import Bookmarks from './pages/bookmarks/bookmarks';
import './app.scss';

import Layout from './layout/Layout';
const App = () => {
  return (
    <div className='App'>
      <Routes>
        <Route element={<Layout />}>
          <Route index element={<Home />} />
          <Route path={`${EPages.characters}`} element={<Characters />} />
          <Route
            path={`${EPages.characters}/:characterId`}
            element={<CharacterDetails />}
          />
          <Route path={`${EPages.comics}`} element={<Comics />} />
          <Route
            path={`${EPages.comics}/:comicId`}
            element={<ComicDetails />}
          />
          <Route path={`${EPages.stories}`} element={<Stories />} />
          <Route
            path={`${EPages.stories}/:storyId`}
            element={<StoryDetails />}
          />
          <Route path={`${EPages.bookmarks}`} element={<Bookmarks />} />
        </Route>
        <Route path='*' element={<PageNotFound />} />
      </Routes>
    </div>
  );
};

export default App;
