import { rest } from 'msw';

import { mockCharacters, mockComics, mockStories } from './mockData';

export const handlers = [
  rest.get(
    'https://gateway.marvel.com/v1/public/characters',
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockCharacters));
    }
  ),
  rest.get(
    'https://gateway.marvel.com/v1/public/characters/*',
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockCharacters));
    }
  ),
  rest.get('https://gateway.marvel.com/v1/public/comics', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockComics));
  }),
  rest.get('https://gateway.marvel.com/v1/public/comics/*', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockComics));
  }),

  rest.get('https://gateway.marvel.com/v1/public/stories', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockStories));
  }),
  rest.get(
    'https://gateway.marvel.com/v1/public/stories/*',
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockStories));
    }
  ),
];
